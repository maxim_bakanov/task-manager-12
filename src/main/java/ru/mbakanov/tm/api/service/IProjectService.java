package ru.mbakanov.tm.api.service;

import ru.mbakanov.tm.model.Project;
import ru.mbakanov.tm.model.Task;

import java.util.List;

public interface IProjectService {
    void create(String name);

    void create(String name, String description);

    void add(Project project);

    void remove(Project project);

    List<Project> findAll();

    void clean();

    Project findOneById(String id);

    Project findOneByName(String name);

    Project findOneByIndex(Integer index);

    Project removeOneById(String id);

    Project removeOneByName(String name);

    Project removeOneByIndex(Integer index);

    Project updateOneById (String id, String name, String description);

    Project updateOneByIndex (Integer index, String name, String description);

}
